sed s/"^chr"/""/g $1 | sort -k1,1V -k2,2n > $2 

bedtools merge -i $2 > $3;

bedtools intersect -b $3 -a $4 -wa -header > $5;

grep -v "^##" $5 | cut -f 1,2,4,5
