# GIAB_bed2vcf and bed2vcf Specification

GIAB_bed2vcf.sh
bed2vcf.sh
check_variants.sh

## Requirement:

Output variants in the form of .bed file from GIAB that intersect within a particular gene.

## Instructions:

### Step 1:

To run the script you need to give the script 6 file paths in the following order after declaring the interpreter and script.

If you DO NOT have GENE.bed file containing the gene interest, Go to Step 3 to obtain it, then return to Step 2.

If merged.sorted.GENE.bed file has already been created, go to Step 4.  

Otherwise, continue.

1. Path to the .bed file for a particular gene of interest (e.g. TSC1.bed)
2. Path to the sorted.GENE.bed file by chromosome number and start position (e.g. sorted.TSC1.bed)
3. Path to the merged.sorted.GENE.bed file, that do not contain any overlapping regions (e.g. merged.sorted.TSC1.bed)
4. Path to the GIAB.vcf file containing all variants from WGS (e.g. variants_GIAB/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf)
5. Path to the gene_intersect.vcf file containing GIAB variants(in vcf format) of the regions specified in the merged.sorted.bed file (e.g. TSC1_intersect.vcf)
6. Path to the gene_intersect.bed file containing GIAB variants(in bed format) of the gene (e.g. TSC1_intersect.bed)

### Step 2:

To execute the script, navigate to the directory containing the script and type the following
```Bash
$ bash GIAB_bed2vcf.sh GENE.bed sorted.GENE.bed merged.sorted.GENE.bed GIAB.vcf GENE_intersect.vcf GENE_intersect.bed
```
### Step 3:

To download the GENE.bed file, visit https://genome.ucsc.edu/cgi-bin/hgTables, and choose 

clade:Mammal 
genome:Human
assembly:GrCh37
group:Genes and Gene Predictions
track: NCBI RefSeq
table: RefSeq All
region: genome
identifiers: paste list (enter the name of the gene you are interested or the transcript you are interested)
output format: BED
output file: GENE.bed

click on "Exons plus" and type "5" bases at each end

and click "get BED", copy the GENE.bed file to the directory where the script is stored (e.g. /mnt/storage/home/zhengt/lab_work/variants_GIAB)

### Step 4:

If you already have the merged.sorted.GENE.bed file, and the GIAB.vcf file you want to compare it with, type the following in command line:

```Bash
$ module load ccbgtool-box
$ bash bed2vcf.sh merged.sorted.GENE.bed GIAB.vcf
```

You should get an output on screen with the intersecting variants displayed similar to the follows:

#CHROM  POS     REF     ALT
9       135767943       C       T
9       135770134       G       A
9       135770300       G       A
9       135770347       A       C
9       135771332       GA      G

If NONE exist, then only the header is displayed, like this:

#CHROM  POS     REF     ALT

### Step 5:

Perform a sanity check on the intersecting by visualising inspecting each variant against merged.sorted.GENE.bed file.

Then, run the following script to see if the variants displayed are found in the GIAB.vcf.

$bash check_variants.sh GENE_intersect.txt GIAB.vcf

The output should look like an vcf file, but with only intersecting variants displayed.

Notes: Be careful to enter bed file and GIAB.vcf in the correct order when typing these arguments in terminal.


## References:

https://bedtools.readthedocs.io/en/latest/content/tools/intersect.html

https://bedtools.readthedocs.io/en/latest/content/tools/merge.html

https://github.com/genome-in-a-bottle/giab_latest_release

https://genome.ucsc.edu/cgi-bin/hgTables
